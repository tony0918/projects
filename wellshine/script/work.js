// JavaScript Document
function work_open(){
	$('#work').stop(true,false).animate({height:"3117px"},time);
	w=setInterval(work_next,8000);
	$('#work_banner .work_banner').show();
	$('#work_content').fadeIn(time);
}

function work_close(){
	$('#work').stop(true,false).animate({height:"87px"},time);
	$('#work_content').fadeOut(time);
	$('#work_banner .work_banner').hide();
	clearInterval(w);
	$('#work_banner .banner_pic ul li img:not(:first)').css({"opacity":"0","width":"0"});
	$('#work_banner .banner_pic ul li img:first').css({"opacity":"1","width":"990px"});
	$('#work_banner .dot img:not(:first)').css({"bottom":"13px"});
	$('#work_banner .dot img:first').css({"bottom":"0"});

}


$(function(){ 
	$('#work_banner').mouseover(function(){
		clearInterval(w);
	}).mouseout(function(){
		w=setInterval(work_next,8000);
	})
})

$(function(){
	$('#work_banner .dot img:first').css({"bottom":"0"});
	$('#work_banner .banner_pic ul li img:not(:first)').css({"opacity":"0","width":"0"});


	$('#work_banner .banner_bottom img:first').click(function(){
		if(!$('#work_banner .banner_pic img').is(":animated")){
			work_previous();}else{return false;}
		})
	$('#work_banner .banner_bottom img:last').click(function(){
		if(!$('#work_banner .banner_pic img').is(":animated")){
			work_next();}else{return false;}
		})
})
//START SCRIPT

function work_next(){
	for(i=0;i<$('#work_banner .banner_pic img').length;i++){
		var op=$('#work_banner .banner_pic img').eq(i).css("opacity");
		if(op!="0"){
			if(i<$('#work_banner .banner_pic img').length-1){
				$('#work_banner .banner_pic img').eq(i).animate({opacity:"0",width:"0"},time);
				$('#work_banner .banner_pic img').eq(i+1).animate({opacity:"1",width:"990px"},time)
				$('#work_banner .dot img').eq(i).css({bottom:"13px"});
				$('#work_banner .dot img').eq(i+1).css({bottom:"0"})	
			}else{
				$('#work_banner .banner_pic img').eq(i).animate({opacity:"0",width:"0"},time);
				$('#work_banner .banner_pic img').eq(0).animate({opacity:"1",width:"990px"},time)
				$('#work_banner .dot img').eq(i).css({bottom:"13px"});
				$('#work_banner .dot img').eq(0).css({bottom:"0"})		
			}}}}
//work_next SCRIPT

function work_previous(){
	for(i=$('#work_banner .banner_pic img').length-1;i>=0;i--){
		var op=$('#work_banner .banner_pic img').eq(i).css("opacity");
		if(op!="0"){
			if(i!=0){
				$('#work_banner .banner_pic img').eq(i).animate({opacity:"0",width:"0"},time);
				$('#work_banner .banner_pic img').eq(i-1).animate({opacity:"1",width:"990px"},time)
				$('#work_banner .dot img').eq(i).css({bottom:"13px"});
				$('#work_banner .dot img').eq(i-1).css({bottom:"0"})
			}else{
				$('#work_banner .banner_pic img').eq(i).animate({opacity:"0",width:"0"},time);
				$('#work_banner .banner_pic img').eq($('#work_banner .banner_pic img').length-1).stop(true,false).animate({opacity:"1",width:"990px"},time)
				$('#work_banner .dot img').eq(i).css({bottom:"13px"});
				$('#work_banner .dot img').eq($('#work_banner .banner_pic img').length-1).css({bottom:"0"})		
			}}}}
//work_previous SCRIPT	

$(function dot(){
	$('#work_banner .dot img').click(function(){
		if(!$('#work_banner .banner_pic img').is(":animated")){
			i=$('#work_banner .dot img').index(this);
			for(k=0;k<=$('#work_banner .banner_pic img').length;k++){
				$('#work_banner .banner_pic img').eq(k).stop(true,false).animate({opacity:"0",width:"0"},time);
				$('#work_banner .banner_pic img').eq(i).stop(true,false).animate({opacity:"1",width:"990px"},time)
				$('#work_banner .dot img').eq(k).stop(true,false).css({bottom:"13px"});
				$('#work_banner .dot img').eq(i).stop(true,false).css({bottom:"0"})
			}}})})
//DOT SCRIPT

$(function(){ 
	$('#our_work .exp ul li img').mouseover(function(){
		var i=$('.exp ul li img').index(this);
		$(this).stop(true,false).animate({bottom:"18px",right:"18px"},200)
	}).mouseout(function(){
		$(this).stop(true,false).animate({bottom:"0",right:"0"},200)
	})
	$('#our_work .exp ul li img').mouseover(function(){
		var t=$(this).attr("title");
		$(this).attr("title","");
	}).mouseout(function(){
		$(this).attr("title",t);
	})
})

$(function(){ 
	$('.logo_c ul li').eq(6).css("opacity","0");
	$('#client .logo ul li').mouseenter(function(){
		var i=$('.logo ul li').index(this);
		if(i==6){$('.logo_c ul li').eq(6).stop(true,false).animate({opacity:"1"},200);}
		$(this).stop(true,false).animate({opacity:"0"},200)
	}).mouseleave(function(){
		$('.logo_c ul li').eq(6).stop(true,false).animate({opacity:"0"},200);
		$(this).stop(true,false).animate({opacity:"1"},200)
	})
})
//LOGO SCRIPT

$(function(){
	$('.exp ul li img,.lasted ul li img').click(function(){
		var ll=(document.documentElement.clientWidth-990)/2;
		var tt=(document.documentElement.clientHeight-580)/2;
		var ss=document.documentElement.scrollTop + document.body.scrollTop;
		var i=$('.exp ul li img').index(this);
		var src=$(this).attr("src").replace("thumb.jpg","preview.jpg");
		if($(this).attr('video')){
			$('body').prepend("<div class='prj_pic_bg'></div><div class='prj_pic'><div id='video-wapper'>" + $(this).attr('video') +"<img src='img/cha.png' id='closeBtn'></div></div>");
		}else{
			$('body').prepend("<div class='prj_pic_bg'></div><div class='prj_pic'><img src='"+src+"'></div><div class='prj_pic_cha'><img src='img/cha.png'></div>");
		}
		$('.prj_pic_bg').css({"top":ss+"px"});
		$('.prj_pic').css({"left":ll+"px","top":ss+tt+"px"});
		$('.prj_pic_cha').css({"right":ll+"px","top":ss+tt+10+"px"});
		$('.prj_pic_bg').css({"opacity":"0.6"});
		$('.prj_pic_bg,.prj_pic').fadeIn(time);
		$('.prj_pic_bg,.prj_pic_cha').click(function(){
			$('.prj_pic,.prj_pic_bg,.prj_pic_cha').fadeOut(time,function(){$('.prj_pic,.prj_pic_bg,.prj_pic_cha').remove()});
		});
		$('#closeBtn').click(function(){
			$('.prj_pic,.prj_pic_bg,.prj_pic_cha').remove();
		});
	})
})