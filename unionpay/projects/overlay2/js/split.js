var dict = "";
var lastword = "";

$.ajax('https://online.unionpay.com/static/cms/js/20/93e40608-c24d-4017-95bf-f9f540ca7aa9.js', {
    'complete': function(jqXHR, textStatus) {
        dict = jqXHR.responseText + "";
    }
});

function divide(text) {
    if (text.length == 0)
        return true;
    var word = text.substring(0, 1) + "";
    var regExp = /\w/;
    
    if (regExp.test(word)) {
        var tmp = text.replace(/^\s*(\w+)\s*.*$/, "$1");


        text = text.replace(/^\s*\w+\s*/, "");
        rs.push(tmp);
        divide(text);
        return;
    }

    var words = [];
    var end = 0;
    var start = -1;

    while ((start = dict.indexOf('\r\n' + word, end)) != -1) {
        
        end = dict.indexOf('\r\n', start + 1);
        if (start == -1 || end == -1)
            return false;
        if (start > end)
            return false;
        words.push(dict.substr(start, end - start).replace(/(\r\n|\s)/g, ""));

    }

    var tmp = "";
    for (j = 0; j < words.length; j++) {
        if (text.indexOf(words[j]) != -1 && words[j].length > tmp.length) {
            tmp = words[j];
        }
    }

    if (tmp == "") {
        tmp = word;
    }

    text = text.replace(tmp, "");
    if (tmp.replace(/\s/g, '') != "")
        rs.push(tmp);

    divide(text);
}

function dodivde() {
    var text = $('#search-input').val();
    rs = [];
    divide(text);
}

$(function() {

    var letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'OTHER'];
    var bar = [];
    _.each(letters, function(element, index) {
        if('OTHER' == element){
            bar[index] = '<a class="last" href="#' + element + '" >' + element + '</a>';
        }else if ('H' == element) {
            bar[index] = '<a class="ml_s" href="#' + element + '" >' + element + '</a>';
        }else if ('P' == element) {
            bar[index] = '<a class="ml_s" href="#' + element + '" >' + element + '</a>';
        }else if ('Z' == element) {
            bar[index] = '<a class="ml_s" href="#' + element + '" >' + element + '</a>';
        }else{
            bar[index] = '<a href="#' + element + '">' + element + '</a>';
        }
    });

    $(bar.join('')).appendTo('#bar-section');

    _.each(letters, function(element, index) {
        var content = [];
        _.each(alphabet[element],function(ele,i){
            if(ele['url']){
                content[i] = '<li><a href="http://' + ele['url'] + '" target="_blank">' + ele['name'] + '</a></li>';
            }else{
                content[i] = '<li>' + ele['name'] + '</li>';
            }
        });

        if('OTHER' == element){
            var title = '其他';
            var html = '<div class="block"><div class="title cf"><div class="sp line unit"></div><span class="unit" id="' 
        + element + '">' + title + '</span><div class="sp line unit"></div></div><div class="py_item"><ul class="cf">' 
        + content.join('') + '</ul></div></div>';
        }else{
            var title = element;
            var html = '<div class="block"><div class="title cf"><div class="line unit"></div><span class="unit" id="' 
        + element + '">' + title + '</span><div class="line unit"></div></div><div class="py_item"><ul class="cf">' 
        + content.join('') + '</ul></div></div>';
        }

        $(html).appendTo('.py_cotent_block');
    });

    $(function(){
        $('#btn-go').click(function(event){
            $('#search-result').remove();

            var content = [];
            var userInput = $('#search-input').val();
            if(_.has(forsearch,userInput) && forsearch[userInput].length == 1){
                var directMatch = forsearch[userInput][0];
                if(directMatch['url']){
                    content.push('<li><a href="http://' + directMatch['url'] + '" target="_blank">' + directMatch['name'] + '</a></li>');
                }else{
                    content.push('<li>' + directMatch['name'] + '</li>');
                }
            }else{
                dodivde();
                var userResultName = [];
                _.each(rs,function(ele){
                    var k = forsearch[ele];
                    _.each(k,function(e){
                        if(!_.contains(userResultName, e['name'])){
                            userResultName.push(e['name']);
                            if(e['url']){
                                content.push('<li><a href="http://' + e['url'] + '" target="_blank">' + e['name'] + '</a></li>');
                            }else{
                                content.push('<li>' + e['name'] + '</li>');
                            }
                        }
                    });
                });
            }

            $('#bar-section').hide();
            $('.py_cotent_block .block').hide();
            var html = '<div class="block" id="search-result"><div class="title cf"><div class="sp line unit"></div><span class="unit">结果</span><div class="sp line unit"></div></div><div class="py_item"><ul class="cf">' 
                + content.join('') + '</ul></div></div>';
            $(html).appendTo('.py_cotent_block');

        });

        $('#search-input').keyup(function() {
            var v = $(this).val();
            if(!v){
                $('#search-result').remove();
                $('#bar-section').show();
                $('.py_cotent_block .block').show();
            }
        });


    });

});