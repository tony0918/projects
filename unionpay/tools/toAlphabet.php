<?php

// Create json file for all items.

require_once '../libs/medoo.min.php';

$database = new medoo('test');
$datas = $database->select('data_convert', array('name', 'url', 'prefix'), array('ORDER' => 'prefix ASC', 'GROUP' => 'name'));
$result = array();

foreach ($datas as $key => $value) {
	$matches = array();
	if(preg_match_all('/[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+/i', $value['url'], $matches)){
		$tmp = array_pop($matches);
		$value['url'] = array_pop($tmp);
	}else{
		$value['url'] = 0;
	}
  if (preg_match("/^[A-Za-z]+/i", $value['prefix'])) {
    $result[strtoupper($value['prefix'])][] = $value;
  } else {
    $result['OTHER'][] = $value;
  }
}

$f = fopen('alphabet.js', 'w');
$flag = fwrite($f, json_encode($result));
fclose($f);
print_r('DONE');
print_r($result);
exit;



// foreach ($datas as $key => $value) {
// 	$result[] = $value['name'];
// }

// $f = fopen('all.js', 'w');
// $txt = 'total = '.json_encode($result);
// $flag = fwrite($f, $txt);
// fclose($f);
// print_r('DONE');
// exit;