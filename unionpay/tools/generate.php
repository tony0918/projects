<?php
// Create result Excel file.
date_default_timezone_set('UTC');

require_once 'medoo.min.php';
require_once 'PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

$database = new medoo('test');
$sql = "SELECT * FROM data_convert ORDER BY prefix ASC,id ASC";
$datas = $database->query($sql)->fetchAll();

$objPHPExcel = new PHPExcel();
$objPHPExcel->createSheet();
$sheet = $objPHPExcel->getActiveSheet();
$sheet->setCellValueByColumnAndRow(0, 1,'商户号');
$sheet->setCellValueByColumnAndRow(1, 1,'商户名称');
$sheet->setCellValueByColumnAndRow(2, 1,'收单机构号');
$sheet->setCellValueByColumnAndRow(3, 1,'收单机构名称');
$sheet->setCellValueByColumnAndRow(4, 1,'网址');
$sheet->setCellValueByColumnAndRow(5, 1,'prefix');

foreach ($datas as $key => $value) {
	for ($i=0; $i < 6; $i++) { 
		$sheet->setCellValueByColumnAndRow($i, $key+2,$value[$i]);
	}
}


$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$writer->save('data2.xls');

echo 'OK';
?>