<?php
// Import from Excel file.

date_default_timezone_set('UTC');

require_once '../libs/medoo.min.php';
require_once '../libs/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';
require_once 'test.php';

$objReader = new PHPExcel_Reader_Excel2007();
$objPHPExcel = $objReader->load('../assets/data.xlsx');

$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
$size = count($sheetData);

$database = new medoo('test');
for ($i=1; $i <= $size; $i++) {
	
	if(iconv('UTF-8','GB2312',$sheetData[$i]['B'])){
		$txt = substr(c(iconv('UTF-8','GB2312',$sheetData[$i]['B'])),0,1);
	}else{
		echo $sheetData[$i]['A'];
		$txt = '';
	}

	$database->insert('data_convert',[
		'id'=>$sheetData[$i]['A'],
		'name'=>$sheetData[$i]['B'],
		'agency_number'=>$sheetData[$i]['A'],
		'agency_name'=>$sheetData[$i]['B'],
		'url'=>$sheetData[$i]['C'],
		'prefix'=>$txt
	]);
}

echo 'OK';
exit;
?>